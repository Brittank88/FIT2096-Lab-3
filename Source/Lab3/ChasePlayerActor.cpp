// Fill out your copyright notice in the Description page of Project Settings.


#include "ChasePlayerActor.h"
#include "UObject/ConstructorHelpers.h"

// Sets default values
AChasePlayerActor::AChasePlayerActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	VisibleComponent->SetupAttachment(RootComponent);

	ConstructorHelpers::FObjectFinder<UStaticMesh> CubeMeshObj(TEXT("/Game/Geometry/Meshes/1M_Cube.1M_Cube"));

	if (CubeMeshObj.Succeeded())
	{
		VisibleComponent->SetStaticMesh(CubeMeshObj.Object);
	}

	Speed = 200.f;

	MinimumDistance = 150.f;
}

// Called when the game starts or when spawned
void AChasePlayerActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AChasePlayerActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (FollowTarget != nullptr) {
		FVector TargetDirection = FollowTarget->GetActorLocation() - GetActorLocation();

		if (TargetDirection.Size() > MinimumDistance) {
			TargetDirection.Normalize();
			SetActorLocation(GetActorLocation() + (TargetDirection * Speed * DeltaTime));
		}
	}
}

