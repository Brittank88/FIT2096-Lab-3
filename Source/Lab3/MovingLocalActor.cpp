// Fill out your copyright notice in the Description page of Project Settings.


#include "MovingLocalActor.h"
#include "UObject/ConstructorHelpers.h"

// Sets default values
AMovingLocalActor::AMovingLocalActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	VisibleComponent->SetupAttachment(RootComponent);

	ConstructorHelpers::FObjectFinder<UStaticMesh> CubeMeshObj(TEXT("/Game/Geometry/Meshes/1M_Cube.1M_Cube"));

	if (CubeMeshObj.Succeeded())
	{
		VisibleComponent->SetStaticMesh(CubeMeshObj.Object);
	}

	TravelDistance = 2000.f;
	Speed = 200.f;
	Tolerance = 10.f;
	Direction = 1.f;
}

// Called when the game starts or when spawned
void AMovingLocalActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMovingLocalActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// GetActorForwardVector gets us the LOCAL forward of the object. This is the forward with rotation applied
	// Direction is used to control forward or back
	FVector DirectionVector = GetActorForwardVector() * Direction;
	FVector CurrentPosition = GetActorLocation();
	// We calculate target position here instead. Ultimately little difference between this and World Actor
	FVector TargetPosition = StartingPosition + (DirectionVector * TravelDistance);
	// Increment position
	CurrentPosition += DirectionVector * Speed * DeltaTime;
	// Same as World Actor
	if (FVector::Dist(CurrentPosition, TargetPosition) <= Tolerance)
	{
		CurrentPosition = TargetPosition;
		StartingPosition = CurrentPosition;
		Direction *= -1;
	}
	SetActorLocation(CurrentPosition);

}

