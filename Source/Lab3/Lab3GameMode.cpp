// Copyright Epic Games, Inc. All Rights Reserved.

#include "Lab3GameMode.h"
#include "Lab3Character.h"
#include "UObject/ConstructorHelpers.h"

ALab3GameMode::ALab3GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
