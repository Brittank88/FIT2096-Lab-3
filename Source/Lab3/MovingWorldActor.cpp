// Fill out your copyright notice in the Description page of Project Settings.


#include "MovingWorldActor.h"
#include "UObject/ConstructorHelpers.h"

// Sets default values
AMovingWorldActor::AMovingWorldActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	VisibleComponent->SetupAttachment(RootComponent);

	ConstructorHelpers::FObjectFinder<UStaticMesh> CubeMeshObj(TEXT("/Game/Geometry/Meshes/1M_Cube.1M_Cube"));

	if (CubeMeshObj.Succeeded())
	{
		VisibleComponent->SetStaticMesh(CubeMeshObj.Object);
	}

	TravelDistance = 2000.f;
	Speed = 200.f;
	Tolerance = 10.f;
	Direction = 1.f;
}

// Called when the game starts or when spawned
void AMovingWorldActor::BeginPlay()
{
	Super::BeginPlay();
	
	// Create a vector that will be our travel direction and distance
	// As this is world space we use 1 on the X Axis for forward.
	// Multiplying by TravelDistance gives us the distance we expect
	const FVector TravelDirection = FVector(1, 0, 0) * TravelDistance;

	// Our start position is wherever we started in the world (May not be 0,0,0)
	StartingPosition = GetActorLocation();
	// Target position is equal to start + travel direction. Remember VectorAddition
	TargetPosition = StartingPosition + TravelDirection * Direction;
}

// Called every frame
void AMovingWorldActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// This is the vector we use to determine which way we will move
	// Default is forward on the X Axis. If Direction is -1 then it is negated to go backwards!
		FVector DirectionVector = FVector(1, 0, 0) * Direction;
	FVector CurrentPosition = GetActorLocation();
	// Update our current position to move incrementally. Speed * Timestep makes us move at a steady pace
		// More on this in Week 4 & 5
		CurrentPosition += DirectionVector * Speed * DeltaTime;
	// If we are within our tolerance distance then we are "close enough"
	// The Dist Function gets a vector between the two (Subtraction)
	// and then gets its magnitude!
	if (FVector::Dist(CurrentPosition, TargetPosition) <= Tolerance)
	{
		// We are at our target location now
		// Tolerance shouldnt be too high or this will appear to "snap"
		CurrentPosition = TargetPosition;
		// We are now moving towards our start position
		TargetPosition = StartingPosition;
		// We are "starting" from our current location
		StartingPosition = CurrentPosition;
		// Negate direction. Will either be 1 or -1 depending on which way we are going
		Direction *= -1;
	}
	// Set the new location
	SetActorLocation(CurrentPosition);
}

