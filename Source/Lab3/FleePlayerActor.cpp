// Fill out your copyright notice in the Description page of Project Settings.


#include "FleePlayerActor.h"

// Sets default values
AFleePlayerActor::AFleePlayerActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	VisibleComponent->SetupAttachment(RootComponent);

	ConstructorHelpers::FObjectFinder<UStaticMesh> CubeMeshObj(TEXT("/Game/Geometry/Meshes/1M_Cube.1M_Cube"));

	if (CubeMeshObj.Succeeded())
	{
		VisibleComponent->SetStaticMesh(CubeMeshObj.Object);
	}

	Speed = 500.f;

	MinimumDistance = 500.f;
}

// Called when the game starts or when spawned
void AFleePlayerActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFleePlayerActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (FleeTarget != nullptr) {
		FVector TargetDirection = GetActorLocation() - FleeTarget->GetActorLocation();

		if (TargetDirection.Size() < MinimumDistance) {
			TargetDirection.Normalize();
			TargetDirection.SetComponentForAxis(EAxis::Z, 0.f);
			SetActorLocation(GetActorLocation() + (TargetDirection * Speed * DeltaTime));
		}
	}
}

