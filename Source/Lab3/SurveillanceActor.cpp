// Fill out your copyright notice in the Description page of Project Settings.


#include "SurveillanceActor.h"

// Sets default values
ASurveillanceActor::ASurveillanceActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	VisibleComponent->SetupAttachment(RootComponent);

	ConstructorHelpers::FObjectFinder<UStaticMesh> CubeMeshObj(TEXT("/Game/Geometry/Meshes/1M_Cube.1M_Cube"));

	if (CubeMeshObj.Succeeded())
	{
		VisibleComponent->SetStaticMesh(CubeMeshObj.Object);
	}

	ConstructorHelpers::FObjectFinder<UMaterial> OffMaterialObj
	(TEXT("/Game/StarterContent/Materials/M_Tech_Hex_Tile.M_Tech_Hex_Tile"));
	ConstructorHelpers::FObjectFinder<UMaterial> OnMaterialObj
	(TEXT("/Game/StarterContent/Materials/M_Tech_Hex_Tile_Pulse.M_Tech_Hex_Tile_Pulse"))
		;
	if (OffMaterialObj.Succeeded())
	{
		OffMaterial = OffMaterialObj.Object;
	}
	if (OnMaterialObj.Succeeded())
	{
		OnMaterial = OnMaterialObj.Object;
	}

	MaxDistance = 500.f;
}

// Called when the game starts or when spawned
void ASurveillanceActor::BeginPlay()
{
	Super::BeginPlay();
	
	VisibleComponent->SetMaterial(0, OffMaterial);
}

// Called every frame
void ASurveillanceActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (SurveillanceTarget != nullptr) {
		FVector SurveillanceDirection = SurveillanceTarget->GetActorLocation() - GetActorLocation();

		if (SurveillanceDirection.Size() < MaxDistance) {
			SurveillanceDirection.Normalize();
			float DotProduct = FVector::DotProduct(GetActorForwardVector(), SurveillanceDirection);

			VisibleComponent->SetMaterial(0, DotProduct > 0 ? OnMaterial : OffMaterial);
		}
	}
}

